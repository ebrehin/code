Welcome to our data analysis project!

If you're here, it means you're already familiar with this project, so I'll just provide instructions on how to launch it.

REQUIREMENTS:

1. Windows or Windows with WSL, or Linux
2. PHP
3. SQL database (I personally use PostgreSQL)
4. Your brain (yes, my code isn't perfect, glhf!)
5. A reasonably powerful computer; for large datasets (up to 1000 MAX), it takes approximately 8 minutes to load with my AMD Ryzen 5 5700X, so a better computer is preferable.
6. A free connection to DBLP! If your network has restrictions, this code may not perform efficiently!

HOW TO LAUNCH IT:

1. You need to modify `connectbdd.php` with your personal database setup :
   Complete the variables host, dbname, user and password lines 4 to 7.
2. Try running the script `create_db_and_run`
   - For Windows users, use the `.bat` file.
   - For Linux users, use the `.sh` file.
3. If the first script doesn't work:
   You have to create the database manually, then use the script `bddexe.sql` to create and populate the database.
4. After that, use the second script: `MapConferencesExe_[OS].[bat or sh]`.
   It should start the local server on port 8888. If this port is already in use, you need to change the port and open a Google Chrome page.
5. If the fourth step didn't work, you have to launch a local server by yourself and then open the index with your web app (Chrome, Firefox, Edge, etc.).

HOW TO USE IT:

- You can search for a subject using the text input.
- You can choose how many results you want (more results = longer loading time).
- Then click on "Search" to load the data.

Initially, you'll see a map with every conference of the current year (2024 when I created it).
- Each point corresponds to a location hosting a conference, where the width represents the number of conferences in the city, and the color corresponds to the highest rank of the conferences.
- You can navigate through time using the arrows at the bottom.
- You can move around the map (tip: use your keyboard arrows).

If you want to see every conference at the same time, you can click on "Show all spheres"; the height and color depend on the years of the conferences.